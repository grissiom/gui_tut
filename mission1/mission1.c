#include <rtthread.h>
#include <rtgui/rtgui_app.h>
#include <rtgui/widgets/window.h>

int main(void)
{
	struct rtgui_app* app;
	struct rtgui_win* win;

	app = rtgui_app_create("mission1");
	if (app == RT_NULL)
	{
        rt_kprintf("failed to create app\n");
        return 0;
    }

    rtgui_rect_t rect = {70, 70, 320, 250};
    win = rtgui_win_create(RT_NULL, "mission1", &rect,
                           RTGUI_WIN_STYLE_DESTROY_ON_CLOSE | RTGUI_WIN_STYLE_CLOSEBOX);

    rtgui_win_show(win, RT_FALSE);
    rtgui_app_run(app);
    rtgui_app_destroy(app);

    return 0;
}
