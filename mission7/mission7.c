#include <rtthread.h>
#include <rtgui/rtgui_app.h>
#include <rtgui/widgets/window.h>
#include <rtgui/widgets/container.h>
#include <rtgui/widgets/button.h>

static rt_bool_t btn_event_handler(struct rtgui_object *object, struct rtgui_event *event)
{
    rt_bool_t ret;

    ret = rtgui_button_event_handler(object, event);
    if (event->type == RTGUI_EVENT_PAINT)
    {
        struct rtgui_dc *dc;

        dc = rtgui_dc_begin_drawing(RTGUI_WIDGET(object));
        if (dc == RT_NULL)
            return RT_FALSE;
        rtgui_dc_fill_ellipse(dc, 70, 50, 40, 20);
        rtgui_dc_end_drawing(dc);
    }
    return ret;
}

int main(void)
{
	struct rtgui_app *app;
	struct rtgui_win *win;
	struct rtgui_button *btn;

	app = rtgui_app_create("mission7");
	if (app == RT_NULL)
	{
        rt_kprintf("failed to create app\n");
        return 0;
    }

    rtgui_rect_t rect = {70, 70, 320, 250};
    win = rtgui_win_create(RT_NULL, "mission7", &rect,
                           RTGUI_WIN_STYLE_DESTROY_ON_CLOSE | RTGUI_WIN_STYLE_CLOSEBOX);
    rect.x1 += 20;
    rect.y1 += 20;
    rect.x2 -= 20;
    rect.y2 -= 20;

    btn = rtgui_button_create("");
    rtgui_widget_set_rect(RTGUI_WIDGET(btn), &rect);
    rtgui_object_set_event_handler(RTGUI_OBJECT(btn), btn_event_handler);

    rtgui_container_add_child(RTGUI_CONTAINER(win), RTGUI_WIDGET(btn));

    rtgui_win_show(win, RT_FALSE);
    rtgui_app_run(app);
    rtgui_app_destroy(app);

    return 0;
}
