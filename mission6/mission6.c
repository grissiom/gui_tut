#include <rtthread.h>
#include <rtgui/rtgui_app.h>
#include <rtgui/widgets/window.h>
#include <rtgui/widgets/container.h>
#include <rtgui/widgets/button.h>

struct rtgui_win *_mainwin;

static void _on_button(struct rtgui_object *obj, struct rtgui_event *eve)
{
	struct rtgui_win *win;

    rtgui_rect_t rect = {140, 140, 370, 240};
    win = rtgui_win_create(_mainwin, "mission6-2", &rect,
                           RTGUI_WIN_STYLE_DESTROY_ON_CLOSE | RTGUI_WIN_STYLE_CLOSEBOX);
    rtgui_win_show(win, RT_TRUE);
}

int main(void)
{
	struct rtgui_app *app;
	struct rtgui_button *btn;

	app = rtgui_app_create("mission6");
	if (app == RT_NULL)
	{
        rt_kprintf("failed to create app\n");
        return 0;
    }

    rtgui_rect_t rect = {70, 70, 320, 250};
    _mainwin = rtgui_win_create(RT_NULL, "mission6", &rect,
                           RTGUI_WIN_STYLE_DESTROY_ON_CLOSE | RTGUI_WIN_STYLE_CLOSEBOX);
    rect.x1 += 20;
    rect.y1 += 20;
    rect.x2 -= 20;
    rect.y2 -= 20;

    btn = rtgui_button_create("hello");
    rtgui_widget_set_rect(RTGUI_WIDGET(btn), &rect);
    rtgui_button_set_onbutton(btn, _on_button);

    rtgui_container_add_child(RTGUI_CONTAINER(_mainwin), RTGUI_WIDGET(btn));

    rtgui_win_show(_mainwin, RT_FALSE);
    rtgui_app_run(app);
    rtgui_app_destroy(app);

    return 0;
}
