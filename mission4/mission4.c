#include <rtthread.h>
#include <rtgui/rtgui_app.h>
#include <rtgui/widgets/window.h>
#include <rtgui/widgets/container.h>
#include <rtgui/widgets/button.h>

static rt_bool_t btn_event_handler(struct rtgui_object *object, struct rtgui_event *event)
{
    if (event->type == RTGUI_EVENT_PAINT)
    {
        rt_kprintf("*** PAINT on %p\n", object);
    }
    return rtgui_button_event_handler(object, event);
}

int main(void)
{
	struct rtgui_app *app;
	struct rtgui_win *win;
	struct rtgui_button *btn;

	app = rtgui_app_create("mission4");
	if (app == RT_NULL)
	{
        rt_kprintf("failed to create app\n");
        return 0;
    }

    rtgui_rect_t rect = {70, 70, 320, 250};
    win = rtgui_win_create(RT_NULL, "mission4", &rect,
                           RTGUI_WIN_STYLE_DESTROY_ON_CLOSE | RTGUI_WIN_STYLE_CLOSEBOX);
    rect.x1 += 20;
    rect.y1 += 20;
    rect.x2 -= 20;
    rect.y2 -= 20;

    btn = rtgui_button_create("hello");
    rtgui_widget_set_rect(RTGUI_WIDGET(btn), &rect);
    rtgui_object_set_event_handler(RTGUI_OBJECT(btn), btn_event_handler);

    rtgui_container_add_child(RTGUI_CONTAINER(win), RTGUI_WIDGET(btn));

    rtgui_win_show(win, RT_FALSE);
    rtgui_app_run(app);
    rtgui_app_destroy(app);

    return 0;
}
